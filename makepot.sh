#!/bin/sh

cat gamine.desktop.in | grep _ | cut -d "=" --fields=2 | sed 's|^|_("|g' | sed 's|$|")|g' > gamine.desktop.in.tmp

xgettext --language=C++ --keyword=_ --keyword=N_ --output=locale/gamine.pot gamine.c gamine.desktop.in.tmp

rm -f gamine.desktop.in.tmp

sed -i "s|in.tmp|in|g" locale/gamine.pot

for a in locale/*.po; do
msgmerge -U $a locale/gamine.pot
done
