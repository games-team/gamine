DESTDIR = 
PREFIX = /usr
BINDIR = $(PREFIX)/bin
DATADIR = $(PREFIX)/share
PKGDATADIR = $(DATADIR)/gamine
DOCDIR = $(DATADIR)/doc/gamine
SYSCONFDIR = /etc
DESKTOPDIR = $(DATADIR)/applications
ICONDIR = $(DATADIR)/icons/hicolor/scalable/apps
LOCALEDIR = $(DATADIR)/locale
MANDIR = $(DATADIR)/man/man6

CFLAGS = -Wall -g
CPPFLAGS = $(shell pkg-config --cflags gtk+-3.0 cairo glib-2.0 gstreamer-1.0)  -DDATADIR=\""$(PKGDATADIR)"\"  -DLOCALDIR=\""$(LOCALEDIR)"\" -DSYSCONFDIR=\""$(SYSCONFDIR)"\"
LDLIBS = $(shell pkg-config --libs gtk+-3.0 cairo glib-2.0 gstreamer-1.0)  -DDATADIR=\""$(PKGDATADIR)"\"  -DLOCALDIR=\""$(LOCALEDIR)"\" -DSYSCONFDIR=\""$(SYSCONFDIR)"\" -lm
LDFLAGS = -g 
CC = gcc
target = gamine
objs = gamine.o
langs = $(shell ls locale|grep -v pot|cut -d "." --field=1)

$(target): $(objs)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	for lang in $(langs); do msgfmt -c -o locale/$$lang.mo locale/$$lang.po; done
	./gen_desktop.sh

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(CPPFLAGS)
clean:
	rm -f $(target) $(objs) *~ locale/*.mo locale/*~ *.desktop

install:
	mkdir -p $(DESTDIR)$(BINDIR)
	mkdir -p $(DESTDIR)$(PKGDATADIR)/sounds
	mkdir -p $(DESTDIR)$(DOCDIR)
	mkdir -p $(DESTDIR)$(ICONDIR)
	mkdir -p $(DESTDIR)$(DESKTOPDIR)
	mkdir -p $(DESTDIR)$(SYSCONFDIR)
	mkdir -p $(DESTDIR)$(MANDIR)
	for lang in $(langs); do mkdir -p $(DESTDIR)$(LOCALEDIR)/$$lang/LC_MESSAGES; done
	install -m 755 gamine $(DESTDIR)$(BINDIR)/
	install -m 644 pencil.png $(DESTDIR)$(PKGDATADIR)/
	install -m 644 gamine.png $(DESTDIR)$(PKGDATADIR)/
	install -m 644 sounds/* $(DESTDIR)$(PKGDATADIR)/sounds/
	install -m 644 AUTHORS README.pencil README.sounds README.locale README ChangeLog COPYING $(DESTDIR)$(DOCDIR)/
	install -m 644 gamine.conf $(DESTDIR)$(SYSCONFDIR)/
	for lang in $(langs); do install -m 644 locale/$$lang.mo $(DESTDIR)$(LOCALEDIR)/$$lang/LC_MESSAGES/gamine.mo; done
	install -m 644 gamine.desktop $(DESTDIR)$(DESKTOPDIR)/
	install -m 644 gamine.svg $(DESTDIR)$(ICONDIR)/
	install -m 644 gamine.6 $(DESTDIR)$(MANDIR)/

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/gamine
	rm -rf $(DESTDIR)$(PKGDATADIR)
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -f $(DESTDIR)$(DESKTOPDIR)/gamine.desktop
	rm -f $(DESTDIR)$(ICONDIR)/gamine.svg
	rm -f $(DESTDIR)$(MANDIR)/gamine*
	rm -f $(DESTDIR)$(SYSCONFDIR)/gamine.conf
	rm -f $(DESTDIR)$(LOCALEDIR)/*/LC_MESSAGES/gamine.mo
