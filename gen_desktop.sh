#/bin/sh

# This script generates gamine.desktop file using gamine.desktop.in template and po files

rm -f gamine.desktop
echo `ls ./locale|grep .po|grep -v .pot|cut -d "." --fields=1` > ./locale/LINGUAS
msgfmt --desktop -d ./locale --template gamine.desktop.in -o gamine.desktop
rm -f ./locale/LINGUAS
# compatibility with old gettext
if [ ! -f "gamine.desktop" ]
then
   cp -f gamine.desktop.in gamine.desktop
   echo "Desktop file was not localized, please update gettext"
fi
