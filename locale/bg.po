# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Любомир Василев, 2015
msgid ""
msgstr ""
"Project-Id-Version: gamine-game\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-12-10 11:24+0300\n"
"PO-Revision-Date: 2017-09-22 11:27+0000\n"
"Last-Translator: Любомир Василев\n"
"Language-Team: Bulgarian (http://www.transifex.com/Magic/gamine-game/language/bg/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bg\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: gamine.c:184 gamine.c:605 gamine.c:637
#, c-format
msgid "*** Error: %s does not exist ***\n"
msgstr "*** Грешка: %s не съществува ***\n"

#: gamine.c:402
#, c-format
msgid "*** Error: failed to create directory %s ***\n"
msgstr "*** Грешка: неуспешно създаване на папката %s ***\n"

#: gamine.c:409
#, c-format
msgid "*** Error: failed to create file %s ***\n"
msgstr "*** Грешка: неуспешно създаване на файла %s ***\n"

#: gamine.c:474
msgid "Quit: esc | Clear: space | Save: printscr"
msgstr "Изход: esc | Изчиств.: space | Запазв.: printscr"

#: gamine.c:632
msgid ""
"*** Error: coloured cursors with alpha channel aren't supported, using "
"default cursor ***\n"
msgstr "*** Грешка: оцветените показалци с прозрачност не се поддържат, използване на обикновения показалец ***\n"

#: gamine.desktop.in:1
msgid "Game for small children"
msgstr "Игра за малки деца"

#: gamine.desktop.in:2
msgid "Educational game for young children"
msgstr "Образователна игра за малки деца"
