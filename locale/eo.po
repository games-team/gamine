# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Eduardo Trápani <etrapani@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: gamine-game\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-12-10 11:24+0300\n"
"PO-Revision-Date: 2017-09-22 11:27+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>\n"
"Language-Team: Esperanto (http://www.transifex.com/Magic/gamine-game/language/eo/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: eo\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: gamine.c:184 gamine.c:605 gamine.c:637
#, c-format
msgid "*** Error: %s does not exist ***\n"
msgstr "*** Eraro: %s ne ekzistas ***\n"

#: gamine.c:402
#, c-format
msgid "*** Error: failed to create directory %s ***\n"
msgstr "*** Eraro: ne eblis krei la dosierujon %s ***\n"

#: gamine.c:409
#, c-format
msgid "*** Error: failed to create file %s ***\n"
msgstr "*** Eraro: ne eblis krei la dosieron %s ***\n"

#: gamine.c:474
msgid "Quit: esc | Clear: space | Save: printscr"
msgstr "Eliri: esk | Viŝi: spaco | Konservi: ekranpresi"

#: gamine.c:632
msgid ""
"*** Error: coloured cursors with alpha channel aren't supported, using "
"default cursor ***\n"
msgstr ""

#: gamine.desktop.in:1
msgid "Game for small children"
msgstr ""

#: gamine.desktop.in:2
msgid "Educational game for young children"
msgstr ""
