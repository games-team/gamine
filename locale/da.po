# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Rémi Verschelde <akien@mageia.org>, 2014
# scootergrisen, 2019
msgid ""
msgstr ""
"Project-Id-Version: gamine-game\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-12-10 11:24+0300\n"
"PO-Revision-Date: 2019-01-30 02:47+0000\n"
"Last-Translator: scootergrisen\n"
"Language-Team: Danish (http://www.transifex.com/Magic/gamine-game/language/da/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: da\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: gamine.c:184 gamine.c:605 gamine.c:637
#, c-format
msgid "*** Error: %s does not exist ***\n"
msgstr "*** Fejl: %s findes ikke ***\n"

#: gamine.c:402
#, c-format
msgid "*** Error: failed to create directory %s ***\n"
msgstr "*** Fejl: kunne ikke oprette mappen %s ***\n"

#: gamine.c:409
#, c-format
msgid "*** Error: failed to create file %s ***\n"
msgstr "*** Fejl: kunne ikke oprette filen %s ***\n"

#: gamine.c:474
msgid "Quit: esc | Clear: space | Save: printscr"
msgstr "Afslut: esc | Ryd: mellemrum | Gem: printscr"

#: gamine.c:632
msgid ""
"*** Error: coloured cursors with alpha channel aren't supported, using "
"default cursor ***\n"
msgstr "*** Fejl: farvede markører med alfakanal understøttes ikke. Standardmarkøren vil blive brugt ***\n"

#: gamine.desktop.in:1
msgid "Game for small children"
msgstr "Spil til små børn"

#: gamine.desktop.in:2
msgid "Educational game for young children"
msgstr "Pædagogisk spil til de mindste børn"
